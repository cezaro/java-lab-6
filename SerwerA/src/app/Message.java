package app;

import java.io.Serializable;

public class Message implements Serializable {
    private static final long serialVersionUID = -4610506647126792972L;

    public String sourceServer;
    public String destinationServer;
    public String content;

    public Message(String sourceServer, String destinationServer, String content) {
        this.sourceServer = sourceServer;
        this.destinationServer = destinationServer;
        this.content = content;
    }
}
