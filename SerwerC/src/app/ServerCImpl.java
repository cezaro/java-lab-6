package app;

import wsclient.ServerD;

import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

@WebService(endpointInterface = "app.ServerC")
public class ServerCImpl implements ServerC {
    private static String serverName = "ServerC";
    private static String nextServer = "ServerD";
    public ArrayList<Message> messages = new ArrayList<>();

    public ServerD server;

    public ServerCImpl() throws MalformedURLException {
        connect();
    }

    @Override
    public boolean sendMessage(String sourceServer, String destinationServer, String content, boolean broadcast) {
        Message message = new Message(sourceServer, destinationServer, content);

        if(this.serverName.equals(destinationServer) && !broadcast) {
            System.out.println("[LOG U] " + sourceServer + ": " + content);
            messages.add(message);
            return true;
        }

        if(broadcast) {
            messages.add(message);
            System.out.println("[LOG B] " + sourceServer + ": " + content);

            if(sourceServer.equals(this.nextServer)) {
                return true;
            }
        }

        if(this.serverName.equals(sourceServer) && !this.serverName.equals(destinationServer) && !broadcast)
        {
            System.out.println("[LOG] Nie ma takiego serwera: " + sourceServer);
            return false;
        }

        sendToNext(message, broadcast);
        return true;
    }

    private boolean sendToNext(Message message, boolean broadcast) {
        server.sendMessage(message.sourceServer, message.destinationServer, message.content, broadcast);
        System.out.println("[LOG] Send to " + nextServer);
        return true;
    }

    private boolean connect() throws MalformedURLException {
        URL url = new URL("http://localhost:9903/serverD?wsdl");
        QName qname = new QName("http://app/", "ServerDImplService");

        try {
            Service service = Service.create(url, qname);
            server = service.getPort(ServerD.class);

            return true;
        } catch (Exception e) {
            new java.util.Timer().schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {
                            try {
                                connect();
                            } catch (MalformedURLException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }, 1000
            );
        }

        return false;
    }
}