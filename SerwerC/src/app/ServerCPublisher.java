package app;

import javax.xml.ws.Endpoint;
import java.util.Scanner;


public class ServerCPublisher {

    public static void main(String[] args) throws Exception {
        ServerCImpl server = new ServerCImpl();

        Endpoint.publish("http://localhost:9902/serverC", server);

        Scanner reader = new Scanner(System.in);

        while(true) {

            System.out.println("1. Wyślij wiadomość");
            System.out.println("2. Lista odebranych wiadomości");
            System.out.print("Wybierz: ");
            int n = reader.nextInt();
            reader.nextLine();

            boolean broadcast = false;
            String destinationServer = null,
                    message;

            switch (n) {
                case 1: {
                    System.out.print("Broadcast [t/N]: ");
                    String type = reader.nextLine();

                    if (type.equals("t")) {
                        broadcast = true;
                    } else {
                        System.out.print("Nazwa serwera docelowego (Server A|B|C|D): ");
                        destinationServer = reader.nextLine();
                    }

                    System.out.print("Wiadomość: ");
                    message = reader.nextLine();

                    server.server.sendMessage("ServerC", destinationServer, message, broadcast);
                    System.out.println("");
                    break;
                }

                case 2: {
                    for (Message m : server.messages) {
                        System.out.println("Źródło: " + m.sourceServer + " | Wiadomość: " + m.content);
                    }

                    System.out.println("");
                }
            }
        }
    }
}